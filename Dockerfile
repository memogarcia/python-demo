FROM python:3.8

RUN pip install flask

ADD demo-yws/api.py api.py

CMD python api.py